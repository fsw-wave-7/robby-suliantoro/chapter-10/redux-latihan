import logo from './logo.svg';
import Person from './components/person.component'
import Persondua from './components/persondua.component'
import FormPerson from './components/FormPerson'
import './App.css';
import {BrowserRouter as Router, Switch, Route  } from 'react-router-dom'

function App() {
  return (
   <Router>
     <Switch>
       <Route component={Person} path='/' exact/>
       <Route component={Persondua} path='/dua'/>
       <Route component={FormPerson} path='/form'/>
     </Switch>
   </Router>

  );
}

export default App;
