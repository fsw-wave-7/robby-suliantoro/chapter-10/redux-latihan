import { combineReducers } from "redux";
import personReducers from './person'

export default combineReducers({
    personReducers
});
