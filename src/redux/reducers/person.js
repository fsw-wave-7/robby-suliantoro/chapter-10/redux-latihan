import { ADD, INIT, REMOVE} from '../actions/person.js'

const initialState = {
    personlist : [{
        id: 1,
        name: 'Ryan Gosling',
        address: 'Los Felix, California, U.S',
        phoneNumber: '+1-123-123',
        photo: 'https://hewanee.com/wp-content/uploads/2021/04/Sifat-Anjing-Chow-Chow.jpg'
    }]
}
export default function personReducers (state = initialState, action) {
    switch(action.type) {
        case ADD:
            let finalPerson = [...state.personlist, action.payload]
            return {
                ...state,
                personlist: finalPerson
            }
        case INIT:
            console.log('data action', action.payload)
            let finalpayload = [...state.personlist, ...action.payload]
            return {
                ...state,
                personlist: finalpayload
            }
        case REMOVE: 
        return {
            ...state,
            personlist : action.payload
        }
        default:
            return state
    }
}