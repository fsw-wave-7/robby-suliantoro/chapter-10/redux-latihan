const PersonAPI = {
    all: () => Promise.resolve([
        {
            id: 1,
            name: 'Luthfi',
            address: 'Semarang',
            phoneNumber: '081122334455',
            photo: 'https://awsimages.detik.net.id/community/media/visual/2021/06/23/presiden-jokowi-1_169.jpeg?w=650&q=80'
        },
        {
            id: 2,
            name: 'Robby',
            address: 'Riau',
            phoneNumber: '082233445566',
            photo: 'https://pict-a.sindonews.net/dyn/620/pena/news/2021/07/07/15/476782/dijuluki-king-of-silent-jubir-kh-maruf-amin-wapres-bekerja-dalam-senyap-sue.jpg'
        },
        {
            id: 3,
            name: 'Niko',
            address: 'Pekanbaru',
            phoneNumber: '085544332211',
            photo: 'https://upload.wikimedia.org/wikipedia/commons/f/ff/KIM_Mahfud_MD.jpg'
        },
        {
            id: 3,
            name: 'Nikoddsds',
            address: 'Pekanbaru',
            phoneNumber: '085544332211',
            photo: 'https://upload.wikimedia.org/wikipedia/commons/f/ff/KIM_Mahfud_MD.jpg'
        },
    ])
}

export default PersonAPI