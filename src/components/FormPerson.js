import { connect } from 'react-redux'
import {useEffect, useState} from 'react'
import { Card, Container, Row, Col, Button } from "react-bootstrap";
import { useHistory } from 'react-router';

const FormPerson = (props) => {

    const {addPerson, datapersons} = props
    let route = useHistory()

    let [data, setData] = useState({
        nama: '',
        address: '',
        phoneNumber: '081122334455',
        photo: 'https://awsimages.detik.net.id/community/media/visual/2021/06/23/presiden-jokowi-1_169.jpeg?w=650&q=80'
    })

    
   

    // tangkap nilai input yang di dalam form
    const handleFunction = (e) => {
        console.log(e.target.value)
        setData({
            ...data,
            [e.target.name] : e.target.value
        })

        console.log(data)
    }

    
     // masukkan nilai ke dalam redux melalui dispatch
     const tambahPerson = () => {
         console.log('data yang dikirim', data)

         // manggil dispatch
         addPerson(data)
     }

     const pindahHalaman = () => {
         route.push('/')
     }

     const createRow = (row) => {
        return (
          <Row>
            <Col xs={4}>
              <Card className="conatiner-fluid p-4 text-center">
                <Card.Img
                  variant="top"
                  src={row.photo}
                />
                <Card.Body>
                  <Card.Title>{row.name}</Card.Title>
                  <Card.Text>
                    <Row className="my-2">
                      <Col>{row.address}</Col>
                      <Col>{row.phoneNumber}</Col>
                    </Row>
                  </Card.Text>
                  <Button variant="danger">Remove</Button>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        );
      };



    return (
        <>
            <div>
                <input type="text" name="nama" onChange={handleFunction} />
                <input type="text" name="address" onChange={handleFunction}/>
                <button onClick={() => tambahPerson()}>Tambah User</button>
                <button onClick={() => pindahHalaman()}>to Home Page</button>
            </div>

            <Container classname="p-4"> 
            {datapersons.map(p => createRow(p))}</Container>;
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        datapersons: state.personReducers.personlist,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addPerson: (data) => dispatch({
            type: 'person/ADD',
            payload: data
        })
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(FormPerson)