import { Component } from "react";
import { connect } from "react-redux";
import { Card, Container, Row, Col, Button } from "react-bootstrap";
import PersonAPI from '../assets/persons'

class Persondua extends Component {
  createRow = (row) => {
    return (
      <Row>
        <Col xs={4}>
          <Card className="conatiner-fluid p-4 text-center">
            <Card.Img
              variant="top"
              src={row.photo}
            />
            <Card.Body>
              <Card.Title>{row.name}</Card.Title>
              <Card.Text>
                <Row className="my-2">
                  <Col>{row.address}</Col>
                  <Col>{row.phoneNumber}</Col>
                </Row>
              </Card.Text>
              <Button variant="danger">Remove</Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    );
  };
  
  render() {
    const { data } = this.props;
    console.log(data)
    return <Container classname="p-4"> 
        {data.map(p => this.createRow(p))}</Container>;
  }
}

const mapStateToProps = (state) => ({
  data: state.personReducers.personlist,
});

export default connect(mapStateToProps)(Persondua);
