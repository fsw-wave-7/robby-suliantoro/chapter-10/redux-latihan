import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import {BrowserRouter as Router, Switch, Route  } from 'react-router-dom'
import { Provider } from "react-redux";
import store from "./redux";
import Person from './components/person.component'

import '../node_modules/bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
  <Provider store={store}>
   <App/>
  </Provider>,
  document.getElementById("root")
);
